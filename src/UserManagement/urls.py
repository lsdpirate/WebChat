from django.conf.urls import url
from .views import user_registration, user_login, user_logout

urlpatterns = [
    url(r"^registration/", user_registration),
    url(r"^login/", user_login),
    url(r"^logout/", user_logout),
]
