from django.db import models
# from UserManagement.models import ChatUser
from django.contrib.auth.models import User

class Room(models.Model):
    room_name = models.CharField(max_length=20, unique=True)
    room_label = models.CharField(max_length=64)
    room_creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1,
    )

    def __str__(self):
        return "{} --- {}".format(self.room_name, self.room_label)
