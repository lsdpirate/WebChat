from channels import Group
from channels.auth import channel_session_user, channel_session_user_from_http, http_session_user
import json

"""
    Message type codes are only used for outgoing
    messages from the server.
    Messge type codes:

    0 : not used
    1 : user connection
    2 : user disconnection
    3 : user message
"""
MESSAGE_TYPES = {
    "user_connection": 1,
    "user_disconnection": 2,
    "user_message": 3,
}


@channel_session_user_from_http
def ws_add(message):
    user = message.user
    room_name = message["path"].strip("/").split("/")[1]
    rn = "room_" + room_name
    print("WS connection: {} @ {}".format(user.username, rn))
    reply = make_message(message_type=MESSAGE_TYPES["user_connection"],
                           sender=user.username, content="SERVER")
    Group(rn).send(
        {
            "text": json.dumps(reply)
        }
    )
    Group(rn).add(message.reply_channel)


@channel_session_user
def ws_disconnect(message):
    user = message.user
    username = user.username
    room_name = message["path"].strip("/").split("/")[1]
    rn = "room_" + room_name
    reply = make_message(message_type=MESSAGE_TYPES["user_disconnection"],
                         sender=username, content="SERVER")
    Group(rn).send(
        {
            "text": json.dumps(reply),
        }
    )
    Group(rn).discard(message.reply_channel)


@channel_session_user
def ws_message(message):
    user = message.user
    room_name = message["path"].strip("/").split("/")[1]
    message_content = message["text"]
    rn = "room_" + room_name
    print("WS message: {} @ {} wrote: {}".format(
        user.username, rn, message_content))
    reply = make_message(MESSAGE_TYPES["user_message"],
                         user.username, message_content)
    Group(rn).send(
        {
            "text": json.dumps(reply),
        }
    )


def make_message(message_type, sender, content):
    message = {
        "type": message_type,
        "sender": sender,
        "content": content,
    }
    return message
